export const state = () => ({
  list: [],
})

export const mutations = {
  loadWorkCategories(state, payload) {
    state.list = payload
  },
  createWorkCategory(state, payload) {
    state.list.push(payload)
  },
}

export const getters = {
  getWorkCategoryById: (state) => (id) => {
    return state.list.find((workCategory) => {
      return workCategory.id() === id
    })
  },
}
