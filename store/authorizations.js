export const state = () => ({
  list: [],
})

export const mutations = {
  setAuthorizations(state, payload) {
    state.list = payload
  },
  createAuthorization(state, payload) {
    state.list.push(payload)
  },
}
