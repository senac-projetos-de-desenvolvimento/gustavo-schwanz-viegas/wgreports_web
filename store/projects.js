export const state = () => ({
  list: [],
})

export const mutations = {
  loadProjects(state, payload) {
    state.list = payload
  },
  createProject(state, payload) {
    state.list.push(payload)
  },
}
export const getters = {
  getProjectById: (state) => (id) => {
    return state.list.find((project) => project.id() === id)
  },
}
