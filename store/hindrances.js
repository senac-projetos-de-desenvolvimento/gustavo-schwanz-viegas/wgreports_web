export const state = () => ({
  list: [],
})

export const mutations = {
  craeteHindrance(state, payload) {
    state.list.push(payload)
  },
  removeHindrance(state, payload) {
    state.list = state.list.filter((hindrance) => {
      return hindrance.id !== payload
    })
  },
}
