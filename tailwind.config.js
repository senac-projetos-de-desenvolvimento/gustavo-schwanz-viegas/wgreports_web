const colors = require('tailwindcss/colors')
module.exports = {
  theme: {
    colors: {
      primary: colors.slate || colors.blueGray,
      success: colors.lime,
      warning: colors.red,
      info: colors.amber,
      blue: colors.blue,
      purple: colors.purple,
      pink: colors.pink,
      orange: colors.orange,
      white: colors.white,
    },
  },
}
