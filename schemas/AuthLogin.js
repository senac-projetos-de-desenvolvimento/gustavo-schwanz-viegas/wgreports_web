export default class AuthLogin {
  _sessionToken = null
  constructor(auth) {
    this.$auth = auth
  }

  login(payload) {
    this.setSessionToken(payload.token)
    this.$auth.setUser(payload.userInfo)
  }

  sessionToken() {
    return this._sessionToken
  }

  setSessionToken(token) {
    this._sessionToken = token
  }
}
