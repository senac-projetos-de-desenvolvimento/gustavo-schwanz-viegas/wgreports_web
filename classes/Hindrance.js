/* eslint-disable camelcase */
class Hindrance {
  constructor({
    id,
    description,
    isPublic,
    created_at,
    updated_at,
    work_category_id,
    project_id,
    resolved,
    user_id,
  }) {
    this._id = id
    this.userId = user_id
    this.resolved = resolved
    this.description = description
    this.public = isPublic
    this._register = {
      createdAt: created_at,
      updatedAt: updated_at,
    }
    this.workCategory = work_category_id
    this.projectId = project_id
  }

  id() {
    return this._id
  }

  createdAt() {
    return this._register.createdAt
  }

  updatedAt() {
    return this._register.updatedAt
  }
}

module.exports = Hindrance
