class Authorization {
  constructor({ id, name }) {
    this._id = id
    this._name = name
  }

  name() {
    return this._name
  }

  id() {
    return this._id
  }
}

module.exports = Authorization
