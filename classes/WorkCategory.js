/* eslint-disable camelcase */
class WorkCategory {
  constructor({ id, name, created_at, updated_at }) {
    this._id = id
    this._name = name
    this._register = {
      createdAt: created_at,
      updatedAt: updated_at,
    }
  }

  id() {
    return this._id
  }

  name() {
    return this._name
  }

  createdAt() {
    return this._register.createdAt
  }

  updatedAt() {
    return this._register.updatedAt
  }
}

module.exports = WorkCategory