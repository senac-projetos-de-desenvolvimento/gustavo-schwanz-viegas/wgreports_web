class User {
  _id = null
  _email = null
  _authorization = {
    name: null,
    level: null,
  }

  constructor({ id, email, name, createdAt, updatedAt, authorization }) {
    this._id = id
    this._email = email
    this.name = name
    this._register = {
      createdAt,
      updatedAt,
    }
    this._authorization = { ...authorization }
  }

  id() {
    return this._id
  }

  email() {
    return this._email
  }

  authorizationLevel() {
    return this._authorization.level
  }

  authorizationName() {
    return this._authorization.name
  }

  createdAt() {
    return this._register.createdAt
  }

  updatedAt() {
    return this._register.updatedAt
  }
}

module.exports = User
