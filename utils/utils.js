export const getAccessToken = ($auth) => {
  return `bearer ${$auth.strategies.AuthLogin.sessionToken()}`
}
export const getAxiosHeaders = ($auth) => {
  return {
    headers: {
      authorization: getAccessToken($auth),
      'user-id': $auth.user.id(),
    },
  }
}
